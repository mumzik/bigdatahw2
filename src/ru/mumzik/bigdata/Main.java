package ru.mumzik.bigdata;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static final String[] level=new String[]{"EMERG","ALERT","CRIT","ERR" ,"WARNING","NOTICE","INFO" ,"DEBUG"};

    private static final Pattern p=Pattern.compile("^<(?<PRI>\\d+)>\\s\\d+\\s(?<date>\\d{4}-\\d{2}-\\d{2})(?<hours>\\s\\d{2}).*");

    private static void priCount(String fileName) {

        SparkConf sparkConf = new SparkConf().setMaster("local").setAppName("SyslogCounter");

        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        JavaRDD<String> inputFile = sparkContext.textFile(fileName);

        JavaRDD<String> timePriority = inputFile.flatMap(str->
            Arrays.asList(getTimePriId(str)).iterator()
        );

        JavaPairRDD countData = timePriority.mapToPair(t -> new Tuple2(t, 1)).reduceByKey((x, y) -> (int) x + (int) y);

        Random random=new Random();
        int outId=random.nextInt();
        countData.saveAsTextFile("CountData_"+outId);
    }

    private static String getTimePriId(String str) {
        Matcher m=p.matcher(str);
        if (!m.matches()){
            return null;
        }
        return "\"level:"+getLevelByPRI(m.group("PRI"))+" date:"+m.group("date")+" hour:"+m.group("hours")+"\"";
    }

    private static String getLevelByPRI(String pri) {
        int priVal=Integer.parseInt(pri);
        return level[priVal%8];
    }

    public static void main(String[] args) {
       priCount("input.txt");
    }
}